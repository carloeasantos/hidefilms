var storage = chrome.storage.local;

window.onload = function() {

    storage.get('opFilterType', function(items) {
            if(items.opFilterType == 1){
                document.getElementById("opFilterHide").checked=true
                document.getElementById("opFilterShow").checked=false;
            }

            if(items.opFilterType == 2){
                document.getElementById("opFilterHide").checked=false;
                document.getElementById("opFilterShow").checked=true;
            }
    });

    storage.get('legendSn', function(items) {
            if(items.legendSn== 1){
                document.getElementById("legendSn").checked=true;
            }
    });

    storage.get('legendWn', function(items) {
            if(items.legendWn== 1){
                document.getElementById("legendWn").checked=true;
            }
    });

    document.getElementById("apply-button").onclick = function() {

        var opFilterType = getValueOfRadio('opFilterType');
        var legendSn = getValueOfRadio('legendSn');
        var legendWn = getValueOfRadio('legendWn');

        if (typeof legendSn === "undefined") {
            legendSn = 0;
        }

        if (typeof legendWn === "undefined") {
            legendWn = 0;
        }

        var filters = {
            opFilterType:opFilterType,
            legendSn:legendSn,
            legendWn:legendWn
        };

        storage.set(filters, function() {
                console.log('Settings saved');
        });

        chrome.extension.sendMessage({
                type: "apply-filters",
                filters : filters
        });
    }
}
function getValueOfRadio(name)
{
    var rates = document.getElementsByName(name);
    var rate_value;
    for(var i = 0; i < rates.length; i++){
        if(rates[i].checked){
            rate_value = rates[i].value;
        }
    }
    return rate_value;
}