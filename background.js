// listening for an event / one-time requests
// coming from the popup
chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {
        switch(request.type) {
            case "apply-filters":

            if (typeof request.filters === "undefined") {
                doFilter();
            }
            else {
                doFilter(request.filters);
            }

            break;
        }
        return true;
});

chrome.tabs.onUpdated.addListener( function (tabId, changeInfo, tab) {
        if (changeInfo.status == 'complete' /*&& tab.active*/) {
            doFilter();
        }
})
// send a message to the content script
var doFilter = function(filters) {
    if (typeof filters === "undefined") {
        var message = {
            type: "applys-filters"
        }
    }
    else {
        var message = {
            type: "applys-filters",
            filters: filters
        }
    }

    chrome.tabs.getSelected(null, function(tab){
            chrome.tabs.sendMessage(tab.id, message);
    });
}