var storage = chrome.storage.local;

chrome.extension.onMessage.addListener(function(message, sender, sendResponse) {

        switch(message.type) {
            case "applys-filters":

            $('.movie_list_item').show();

            var opFilterType = null;
            var legendSn = null;
            var legendWn = null;

            if (typeof message.filters === "undefined") {
                storage.get('opFilterType', function(items) {

                        opFilterType = items.opFilterType;

                        storage.get('legendSn', function(items) {

                                legendSn = items.legendSn;

                                storage.get('legendWn', function(items) {
                                        legendWn = items.legendWn;
                                        applyFilter(opFilterType,legendSn,legendWn)
                                });
                        });
                });

            }
            else {
                var filters = message.filters;
                opFilterType = filters.opFilterType;
                legendSn = filters.legendSn;
                legendWn = filters.legendWn;
                applyFilter(opFilterType,legendSn,legendWn);
            }
            break;
        }                                   
});

function applyFilter(opFilterType,legendSn,legendWn)
{

    var exFiltro = new RegExp("(.*)filmow.com(.*)filters","i");
    if (exFiltro.test(document.URL)){ //tem filtro?
        
        var exFiltroJaVi = new RegExp("(.*)filmow.com(.*)watchlist","i");
        if (exFiltroJaVi.test(document.URL)){
            if(opFilterType == 1){ //se tem filtro de "quer ver" e a extensão vai esconder, prevalece o filtro do site
                legendWn = 0;
            }
        }
    }
    
    if(opFilterType == 1) {

        if(legendSn == 1){
            $('span .sn').parent().parent().hide('slow');    
        }
        if(legendWn == 1){
            $('span .ws').parent().parent().hide('slow');    
        }

    }
    else {
        $('.movie_list_item').hide();
        if(legendSn == 1){
            $('span .sn').parent().parent().show('slow');    
        }
        if(legendWn == 1){
            $('span .ws').parent().parent().show('slow');    
        }    
    }
}